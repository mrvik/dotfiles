if $CHROMEBOOK == ""
    let mapleader = "+"
endif
nnoremap <F8> :bp <CR>
nnoremap <F9> :bn <CR>
nnoremap <C-N> :tabnew <CR>
nnoremap <silent> <Leader>l :NERDTreeFind<CR>
nnoremap <silent> <Leader>v :NERDTreeToggle<CR>
nnoremap <C-k> :ALEPreviousWrap<CR>
nnoremap <C-j> :ALENextWrap<CR>
nnoremap <C-b> :ALEGoToDefinition<CR>
nnoremap <Leader>o :!google-chrome-unstable '%:p'&>/dev/null <CR><CR>
nnoremap <Leader>si :Silicon %.png
nnoremap <Leader>f :FZF<CR>
nnoremap <Leader>b :Buffers<CR>
nnoremap <silent> <Leader>t :TagbarToggle<CR>
nnoremap <silent> <Leader>rr :!pandoc -o "%.pdf" "%" <CR><CR>

vnoremap <Leader>ss :'<,'>Silicon %-selection.png
vnoremap <Leader>sh :'<,'>SiliconHighlight %-highlighted.png
vnoremap <silent> <Leader>p :call nwtfind#SearchVisualNWT()<CR>

inoremap <C-w> <C-g>u<C-w>
inoremap <C-u> <C-g>u<C-u>

syntax on
filetype plugin indent on
filetype plugin on

set expandtab ts=4 shiftwidth=4 softtabstop=4
set number "Enable numbers
set relativenumber
set hidden " Allow changing buffer w/o saving
set conceallevel=0
set list
set listchars=tab:>-,trail:·
set modeline

if !empty($FZF_PLUGIN)
    set rtp+=$FZF_PLUGIN
endif
execute pathogen#infect()

augroup VimCSS3Syntax
    autocmd!
    autocmd FileType css setlocal iskeyword+=-
augroup END

augroup nord-theme-overrides
    autocmd!
    " autocmd ColorScheme nord highlight Delimiter ctermfg=11 guifg=#D08770
    " autocmd ColorScheme nord highlight jsIdentifierProp cterm=underline gui=underline
augroup END

nnoremap <F10> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>

"set grepprg=grep\ -nH\ $*

" Colorscheme setting
set background=dark
if $COLORTERM == "truecolor" || $COLORTERM == "24bit"
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    set termguicolors
endif

if $TERM =~ "-256color"
    let g:airline_powerline_fonts = 1
    colorscheme nord
    if $CHROMEBOOK == ""
        let g:airline_left_sep=""
        let g:airline_right_sep=""
        let g:airline_left_alt_sep=""
        let g:airline_right_alt_sep=""
    endif
endif

let g:ale_fixers = {
\    'javascript': ['eslint', 'prettier'],
\    'vue': ['eslint'],
\    'svelte': ['eslint', 'prettier'],
\    'css': ['prettier'],
\    'nix': ['nixfmt'],
\}
let g:ale_linters_ignore={
\    'c': ['clangd'],
\    'go': ['typecheck'],
\}
" let g:ale_completion_enabled = 1
let g:ale_echo_msg_error_str="E"
let g:ale_echo_msg_warning_str="W"
let g:ale_echo_msg_info_str="I"
let g:ale_echo_msg_format= '[%severity%] %s [%linter% - %code%]'
let g:ale_c_parse_makefile=1
let g:airline#extensions#ale#enabled = 1
let g:airline#extensions#tabline#enabled = 1
let g:javascript_plugin_jsdoc = 1
let g:vim_markdown_folding_disabled = 1
let NERDTreeAutoDeleteBuffer = 1
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1
let NERDTreeQuitOnOpen = 1
let g:NERDSpaceDelims = 1
let g:user_emmet_mode='a'
let g:sql_type_default = 'pgsql'

"FZF Config
let g:fzf_action={
            \ 'enter': 'edit',
            \ 'ctrl-t': 'tabe',
            \ 'ctrl-x': 'split',
            \ 'ctrl-v': 'vsplit',
            \}

" vim-go config
let g:go_fmt_autosave = 1
let g:go_asmfmt_autosave = 1
" let g:go_gopls_gofumpt = 1
let g:go_highlight_variable_declarations = 1
let g:go_highlight_operators = 1
let g:go_highlight_functions = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_types = 1

" vim-pandoc config (WIP)
let b:pandoc_command_autoexec_command = "Pandoc! pdf"

let g:silicon = {
      \ 'theme':            'TwoDark',
      \ 'font':             'FuraCode Nerd Font',
      \ 'pad-horiz':        40,
      \ 'pad-vert':         50,
      \ 'shadow-color':     '#000000',
      \ 'line-number':      v:true,
      \ 'round-corner':     v:false,
      \ 'window-controls':  v:true,
      \ }

let g:ale_linter_aliases={'vue': ['vue', 'javascript']}
let g:ale_linters = {
            \'vue': ['eslint', 'vls'],
            \'go': ['gofmt', 'go vet', 'gopls', 'golangci-lint'],
\}

" Configure Iris Mail Client
let g:iris_name="Víctor González"
let g:iris_email="mrvikxd@gmail.com"
let g:iris_imap_host="imap.gmail.com"
let g:iris_imap_port="993"
let g:iris_imap_login="mrvikxd@gmail.com"
"SMTP
let g:iris_smpt_host="smtp.gmail.com"
let g:iris_smtp_port="587"

autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
autocmd BufRead,BufNewFile *.scss set filetype=scss.css
autocmd BufRead,BufNewFile *.groff set filetype=groff
au BufRead,BufNewFile *.ex,*.exs set filetype=elixir
au BufRead,BufNewFile *.eex,*.heex,*.leex,*.sface,*.lexs set filetype=eelixir
au BufRead,BufNewFile mix.lock set filetype=elixir

hi Normal ctermbg=NONE guibg=NONE

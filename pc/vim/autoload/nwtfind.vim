function! s:get_visual_selection()
    " Why is this not a built-in Vim script function?!
    let [line_start, column_start] = getpos("'<")[1:2]
    let [line_end, column_end] = getpos("'>")[1:2]
    let lines = getline(line_start, line_end)
    if len(lines) == 0
        return ''
    endif
    let lines[-1] = lines[-1][: column_end - (&selection == 'inclusive' ? 1 : 2)]
    let lines[0] = lines[0][column_start - 1:]
    return join(lines, "\n")
endfunction

function nwtfind#SearchNWT(string)
    let winnr=bufwinnr('^nwt-find$')
    if winnr >-1
        silent! execute winnr.'windo q'
    endif
    silent! topleft new nwt-find
    setlocal buftype=nowrite bufhidden=wipe nobuflisted noswapfile
    silent! resize 10
    execute "$read !nwt-find" shellescape(a:string)
endfunction

function nwtfind#SearchVisualNWT()
    let selection = s:get_visual_selection()
    call nwtfind#SearchNWT(selection)
endfunction

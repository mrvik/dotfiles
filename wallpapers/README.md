# Wallpapers

This wallpaper section is a combination of the following:
- [ArchLinux Artwork](https://sources.archlinux.org/other/artwork)
- [linux.pictures](https://linux.pictures) or [GitLab repo](https://gitlab.com/jstpcs/lnxpcs) (resized)
- [BlackArch Artwork](https://github.com/BlackArch/blackarch-artwork)
- Images captured from my phone

{ config, pkgs, ... }:

let
  unstable = import<unstable> {};
in
{
  nixpkgs.config.allowUnfree = true;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "mrvik";
  home.homeDirectory = "/home/mrvik";

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "22.05";

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
  programs.vim = {
    enable = true;
    plugins = with pkgs.vimPlugins; [
      fzf-vim
    ];
    extraConfig = ''
      source ~/.vimrc
    '';
  };

  programs.gpg.enable = true;
  services.gpg-agent = {
    enable = true;
    pinentryFlavor = "qt";
  };

  home.packages = let
    # Plasma tools
    kdetools = with pkgs.libsForQt5; [
      qtstyleplugin-kvantum
      marble
      ark
      kate
      kdeconnect-kde
      kontact
      kwallet
    ];
    # Python & co.
    pythonPackages = [pkgs.python3] ++ (with pkgs.python3Packages; [
      dbus-python
    ]);
    # Flutter crap
    flutter = with pkgs; [
      unstable.flutter
      clang
      cmake
      ninja
      pkg-config
    ];
  in
    kdetools ++ pythonPackages ++ (with pkgs; [
    # Non plasma but related tools
    syncthingtray
    pinentry_qt

    # Themes
    nordic

    # Tools
    unstable.jetbrains.idea-community
    virt-manager
    virt-viewer

    # Office tools
    libreoffice-qt

    # CLI tools
    fd
    keychain
    neofetch
    duf
    unzip
    shellcheck
    pandoc
    texlive.combined.scheme-small

    # IM
    tdesktop
    discord
  ]);

  services.syncthing = {
    enable = true;
  };
}

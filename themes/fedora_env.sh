# vim: filetype=sh
export GTK2_RC_FILES="/usr/share/themes/Numix/gtk-2.0/gtkrc"
export GTK_THEME="Numix:dark"
export QT_QPA_PLATFORMTHEME=gtk2
export BACKGROUND_IMAGE="$HOME/git/dotfiles/wallpapers/Lines.jpg"

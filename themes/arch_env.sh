# vim: filetype=sh
theme="Arc-Dark/gtk-2.0/gtkrc"
if [ -d "/usr/share/themes" ]; then
    export GTK2_RC_FILES="/usr/share/themes/$theme"
elif [ -d "/usr/local/share/themes" ]; then
    export GTK2_RC_FILES="/usr/local/share/themes/$theme"
fi
export GTK_THEME="Arc-Dark:dark"
export QT_QPA_PLATFORMTHEME=gtk2
if [ -d "/usr/share/backgrounds/archlinux" ]; then
    export BACKGROUND_IMAGE="/usr/share/backgrounds/archlinux/archlinux-simplyblack.png"
else
    export BACKGROUND_IMAGE="$HOME/git/dotfiles/wallpapers/Car dawn.png"
fi

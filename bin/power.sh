#!/bin/zsh

usage(){
    cat << EOF
Syntax: power.sh poweroff|shutdown|suspend|hibernate|reboot
Note that poweroff and shutdown do the same thing
EOF
}

if [[ "$1" == "poweroff" || "$1" == "shutdown" ]]; then
    systemctl poweroff
elif [[ "$1" == "suspend" ]]; then
    locker.sh now
    systemctl suspend
elif [[ "$1" == "hibernate" ]]; then
    locker.sh now
    systemctl hibernate
elif [[ "$1" == "reboot" ]]; then
    systemctl reboot
else
    usage
fi

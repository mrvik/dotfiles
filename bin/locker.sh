#!/usr/bin/env zsh

goautolockpidfile="${XDG_RUNTIME_DIR}/goautolock-${XDG_SESSION_ID}.pid"
local goautolockpid
if [[ -r $goautolockpidfile ]]; then
    goautolockpid=$(cat $goautolockpidfile)
fi
echo $goautolockpid

if [[ "$1" == "now" ]]; then
    if [[ -v $goautolockpid ]]; then
        exec kill -USR1 $goautolockpid
    else
        exec killall -USR1 goautolock
    fi
elif [[ "$1" == "caffeine" ]]; then
    if [[ -v $goautolockpid ]]; then
        exec kill -USR2 $goautolockpid
    else
        exec killall -USR2 goautolock
    fi
else
    exec goautolock \
        --time 60\
        --locker "i3lock"\
        --args "-d -n -c 222222"\
        --notify 40\
        --caffeine 20
fi
